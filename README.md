# dutch-payment-icons

A collection of icon sets of dutch banks and payment providers.

![iDEAL logo](iconset-1/iDEAL.png "iDEAL logo")


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment

`iconset-1` and `iconset-2` sourced from [Hulp bij Marketing](https://hulpbijmarketing.nl/blog/icons-voor-betaalopties-webshop/)
`icons_mollie` sourced from [mollie](https://www.mollie.com/nl/resources)

## License
For open source projects, say how it is licensed.
